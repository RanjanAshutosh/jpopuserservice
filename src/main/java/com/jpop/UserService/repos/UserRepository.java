package com.jpop.UserService.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jpop.UserService.models.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}